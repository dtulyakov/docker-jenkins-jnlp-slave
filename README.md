[![Docker Pulls](https://img.shields.io/docker/pulls/dtulyakov/jenkins-jnlp-slave.svg)][hub]
[![](https://images.microbadger.com/badges/version/dtulyakov/jenkins-jnlp-slave.svg)](https://microbadger.com/images/dtulyakov/jenkins-jnlp-slave "latest")[![](https://images.microbadger.com/badges/image/dtulyakov/jenkins-jnlp-slave.svg)](https://microbadger.com/images/dtulyakov/jenkins-jnlp-slave "latest")


[hub]: https://hub.docker.com/r/dtulyakov/jenkins-jnlp-slave/

```BASH
docker build --force-rm --no-cache --tag=dtulyakov/jenkins-jnlp-slave:latest . \
  && docker create --restart=always \
     --network=host \
     --name=Jenkins-Slave \
     --env=TRY_UPGRADE_IF_NO_MARKER=true \
     -v $(readlink -f /var/run/docker.sock):$(readlink -f /var/run/docker.sock) \
     -v $(which docker):$(which docker) \
     -v ./data/jenkins:/home/jenkins \
     dtulyakov/jenkins-jnlp-slave \
  && docker start Jenkins-Slave

```
