FROM jenkins/jnlp-slave:latest

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

LABEL org.label-schema.build-date=$BUILD_DATE \
  org.label-schema.docker.cmd="docker run dtulyakov/jenkins-jnlp-slave -url http://jenkins-server:port -workDir=/home/jenkins/agent <secret> <agent name>" \
  org.label-schema.description="Jenkins jnlp slave" \
  org.label-schema.name="jenkins-jnlp-slave" \
  org.label-schema.schema-version="4.3.9" \
  org.label-schema.url="https://jenkins.io/" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.vcs-url="https://bitbucket.org/dtulyakov/docker-jenkins-jnlp-slave" \
  org.label-schema.vendor="dtulyakov" \
  org.label-schema.version=$VERSION


USER root

RUN set -x \
  && apt update -qq \
  && apt install --no-install-recommends -qy \
     software-properties-common \
     python-requests \
     python3-pip \
     python3-setuptools \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     rsync \
     ruby \
     ruby-dev \
  && pip3 install pip --upgrade \
  && pip install pep8 \
     pylint \
     pytest \
     pytest-cov \
  && pip3 install s3cmd \
  && apt-add-repository 'deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main' \
  && add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/debian stretch stable' \
  && apt-key adv --no-tty --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367 \
  && apt-key adv --no-tty --keyserver keyserver.ubuntu.com --recv-keys 7EA0A9C3F273FCD8 \
  && apt update -qq \
  && apt install -qy docker-ce \
  && apt autoremove -y \
  && usermod -aG docker jenkins \
  && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*

USER jenkins


# vim: set filetype=dockerfile et sw=2 ts=2:
